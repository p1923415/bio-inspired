# coding: utf8
# !/usr/bin/env python
# ------------------------------------------------------------------------
# Perceptron en pytorch (en utilisant juste les tenseurs)
# Écrit par Mathieu Lefort
#
# Distribué sous licence BSD.
# ------------------------------------------------------------------------

import gzip, numpy, torch

if __name__ == '__main__':
    batch_size = 5  # nombre de données lues à chaque fois
    nb_epochs = 10  # nombre de fois que la base de données sera lue
    eta = 0.01  # taux d'apprentissage
    hidden_layer_size = 64

    # on lit les données
    b = ((data_train, label_train), (data_test, label_test)) = torch.load(gzip.open('mnist.pkl.gz'))

    # on initialise le modèle et ses poids
    w = torch.empty((data_train.shape[1], hidden_layer_size), dtype=torch.float)
    w2 = torch.empty((hidden_layer_size, label_train.shape[1]), dtype=torch.float)

    b = torch.empty((1, hidden_layer_size), dtype=torch.float)
    b2 = torch.empty((1, label_train.shape[1]), dtype=torch.float)

    torch.nn.init.uniform_(w, -0.001, 0.001)
    torch.nn.init.uniform_(w2, -0.001, 0.001)

    torch.nn.init.uniform_(b, -0.001, 0.001)
    torch.nn.init.uniform_(b2, -0.001, 0.001)

    nb_data_train = data_train.shape[0]
    nb_data_test = data_test.shape[0]
    indices = numpy.arange(nb_data_train, step=batch_size)

    for n in range(nb_epochs):
        # on mélange les (indices des) données
        numpy.random.shuffle(indices)
        # on lit toutes les données d'apprentissage
        for i in indices:
            # on récupère les entrées
            x = data_train[i:i + batch_size]
            # on calcule la sortie du modèle
            y = 1 / (1 + torch.exp(-(torch.mm(x, w) + b)))
            outY = torch.mm(y, w2) + b2

            # on regarde les vrais labels
            t = label_train[i:i + batch_size]

            # on met à jour les poids
            grad2 = (t - outY)
            grad = y * (1 - y) * torch.mm(grad2, w2.T)

            w += eta * torch.mm(x.T, grad)
            b += eta * grad.sum(axis=0)

            w2 += eta * torch.mm(y.T, grad2)
            b2 += eta * grad2.sum(axis=0)

        # test du modèle (on évalue la progression pendant l'apprentissage)
        acc = 0.
        # on lit toutes les donnéees de test
        for i in range(nb_data_test):
            # on récupère l'entrée
            x = data_test[i:i + 1]
            # on calcule la sortie du modèle
            y = 1 / (1 + torch.exp(-(torch.mm(x, w) + b)))
            outY2 = torch.mm(y, w2) + b2
            # on regarde le vrai label
            t = label_test[i:i + 1]
            # on regarde si la sortie est correcte
            acc += torch.argmax(outY2, 1) == torch.argmax(t, 1)
        # on affiche le pourcentage de bonnes réponses
        print(acc / nb_data_test)
