# coding: utf8
# !/usr/bin/env python
# ------------------------------------------------------------------------
# Perceptron en pytorch (en utilisant les outils de Pytorch)
# Écrit par Mathieu Lefort
#
# Distribué sous licence BSD.
# ------------------------------------------------------------------------
import gzip
import torch

if __name__ == '__main__':
    batch_size = 5  # nombre de données lues à chaque fois
    nb_epochs = 10  # nombre de fois que la base de données sera lue
    eta = 0.01  # taux d'apprentissage

    # on lit les données
    ((data_train, label_train), (data_test, label_test)) = torch.load(gzip.open('mnist.pkl.gz'))

    # on crée les lecteurs de données
    train_dataset = torch.utils.data.TensorDataset(data_train, label_train)
    test_dataset = torch.utils.data.TensorDataset(data_test, label_test)
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=1, shuffle=False)

    # on initialise le modèle et ses poids
    model = torch.nn.Sequential(torch.nn.Linear(data_train.shape[1], 64),
                                torch.nn.Linear(64, 64),
                                # torch.nn.Sigmoid(),
                                torch.nn.ReLU(),
                                torch.nn.Linear(64, label_train.shape[1]))

    torch.nn.init.xavier_normal_(model[0].weight, gain=1)
    torch.nn.init.uniform_(model[0].bias, -0.001, 0.001)

    # on initialise l'optimiseur
    # loss_func = torch.nn.MSELoss(reduction='sum')
    loss_func = torch.nn.CrossEntropyLoss(reduction='mean')
    # optim = torch.optim.SGD(model.parameters(), lr=eta)
    optim = torch.optim.Adagrad(model.parameters(), weight_decay=0, initial_accumulator_value=0, eps=1e-10,
                                foreach=None, maximize=False)

    for n in range(nb_epochs):
        # on lit toutes les données d'apprentissage
        for x, t in train_loader:
            # on calcule la sortie du modèle
            y = model(x)
            # on met à jour les poids
            loss = loss_func(y, t)
            optim.zero_grad()
            loss.backward()
            optim.step()

        # test du modèle (on évalue la progression pendant l'apprentissage)
        acc = 0.
        # on lit toutes les données de test
        for x, t in test_loader:
            # on calcule la sortie du modèle
            y = model(x)
            # on regarde si la sortie est correcte
            acc += torch.argmax(y, 1) == torch.argmax(t, 1)
        # on affiche le pourcentage de bonnes réponses
        print(acc / data_test.shape[0])
